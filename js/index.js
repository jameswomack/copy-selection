var fakePasteboard = ['foo bar baz qux']

function getNode(){
  return document.getSelection().anchorNode && document.getSelection().anchorNode.parentElement
}

function getBody(){
  return document.body
}

function getSelectedText(){
  return document.getSelection().anchorNode.textContent.trim()
}

function ready(){
  var pasteable = document.querySelector('[data-auto=pasteable]')
  pasteable.addEventListener('dblclick', function(){
    pasteable.innerText = fakePasteboard[fakePasteboard.length - 1]
  })
  document.addEventListener('selectionchange', function() {
    var body = getBody()
    var bodyAutoState = body.getAttribute('data-auto')
    var node = getNode()
    if(!node){
      console.error('Node was invalid')
      return
    }
    if(bodyAutoState !== 'copyable'){
      var nodeAutoState = node.getAttribute('data-auto')
      if(nodeAutoState === 'copyable'){
        return fakePasteboard.push(getSelectedText())
      }
    }else{
      return fakePasteboard.push(getSelectedText())
    }
  }, false)
}

document.addEventListener('DOMContentLoaded', ready, false)
